package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Connect;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Router;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.CreatePublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.Messages;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMainActivity extends AppCompatActivity {

    private ImageView perfilImage;
    private EditText descrip, os, tamano;
    private Button btnActualizar;
    private Router router;
    private String img = "https://images.pexels.com/photos/265658/pexels-photo-265658.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
    private String model ;
    private String ediDes ;
    private String imgPubli ;
    private String editos ;
    private String editTamano;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_main);
        perfilImage = findViewById(R.id.perfilImage);
        descrip = findViewById(R.id.editdescrip);
        os = findViewById(R.id.editos);
        tamano = findViewById(R.id.edittamano);
        btnActualizar = findViewById(R.id.btnUpdate);
        router = Connect.getApiService();
        //cargar los datos a la pantalla
        obtener();
        Glide.with(this).load(imgPubli).into(perfilImage);
        descrip.setText(ediDes);
        os.setText(editos);
        tamano.setText(editTamano);
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (descrip.getText().toString().isEmpty() ||
                        os.getText().toString().isEmpty() ||
                        tamano.getText().toString().isEmpty()){
                    //validar campos
                    Toast.makeText(EditMainActivity.this, "DATOS ACTUALIZADOS", Toast.LENGTH_SHORT).show();
                }else {
                    //enviar los datos
                    CreatePublications createPublications = new CreatePublications(
                            model , descrip.getText().toString().trim(), imgPubli,
                            os.getText().toString(), tamano.getText().toString());
                    Call<Messages> createPublicationsCall = router.CREATE_PUBLICATIONS_CAL(createPublications);
                    createPublicationsCall.enqueue(new Callback<Messages>() {
                        @Override
                        public void onResponse(Call<Messages> call, Response<Messages> response) {
                            if (response.isSuccessful()){
                                String message = response.body().getMessage();
                                Toast.makeText(EditMainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<Messages> call, Throwable t) {
                        }
                    });
                }
            }
        });
    }

    private void obtener(){
        try {
            img = "https://images.pexels.com/photos/265658/pexels-photo-265658.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
            model = getIntent().getExtras().getString("model");
            ediDes = getIntent().getExtras().getString("description");
            imgPubli = getIntent().getExtras().getString("img");
            editos = getIntent().getExtras().getString("os");
            editTamano = getIntent().getExtras().getString("tamano");
        }catch (Exception e){

        }
    }
}