package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Adapter.AdapterRecycler;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Connect;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Router;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.ListPublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.ModelPublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdapterRecycler adapterRecycler;
    private ArrayList<ModelPublications> modelPublications;
    private Router router;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        router = Connect.getApiService();
        Call<ListPublications> listPublicationsCall = router.obtenerDatos();
        listPublicationsCall.enqueue(new Callback<ListPublications>() {
            @Override
            public void onResponse(Call<ListPublications> call, Response<ListPublications> response) {
                if (response.isSuccessful()){
                    recyclerView = findViewById(R.id.recyclerViewHome);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                    recyclerView.setLayoutManager(layoutManager);
                    modelPublications = response.body().getModelPublications();
                    adapterRecycler = new AdapterRecycler(modelPublications, MainActivity.this);
                    recyclerView.setAdapter(adapterRecycler);
                    recyclerView.setHasFixedSize(true);
                }
            }
            @Override
            public void onFailure(Call<ListPublications> call, Throwable t) {
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflar en menu
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.bookmark:
                startActivity(new Intent(MainActivity.this, RegisterMainActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}