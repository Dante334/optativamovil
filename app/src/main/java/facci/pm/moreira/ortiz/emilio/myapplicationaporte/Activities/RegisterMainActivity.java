package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Connect;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Router;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.CreatePublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.Messages;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.ModelPublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterMainActivity extends AppCompatActivity {

    private EditText model, descripcion, img, os, tamano;
    private ModelPublications modelPublications;
    private Button btnEnviar;
    private Router router;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_main);
        model = findViewById(R.id.modelo);
        descripcion = findViewById(R.id.descripcion);
        os = findViewById(R.id.os);
        tamano = findViewById(R.id.tamano);
        btnEnviar = findViewById(R.id.button);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                router = Connect.getApiService();
                String img = "https://images.pexels.com/photos/265658/pexels-photo-265658.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";
                if (model.getText().toString().trim().isEmpty() || descripcion.getText().toString().trim().isEmpty() ||
                        os.getText().toString().trim().isEmpty()  || tamano.getText().toString().trim().isEmpty()){
                    //validar campos
                    Toast.makeText(RegisterMainActivity.this, "not", Toast.LENGTH_SHORT).show();
                }else {
                    //enviar los datos
                    CreatePublications createPublications = new CreatePublications(
                            model.getText().toString().trim(), descripcion.getText().toString().trim(), img,
                            os.getText().toString(), tamano.getText().toString());
                    Call<Messages> createPublicationsCall = router.CREATE_PUBLICATIONS_CAL(createPublications);
                    createPublicationsCall.enqueue(new Callback<Messages>() {
                        @Override
                        public void onResponse(Call<Messages> call, Response<Messages> response) {
                            if (response.isSuccessful()){
                                String message = response.body().getMessage();
                                Toast.makeText(RegisterMainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                                model.setText("");
                                descripcion.setText("");
                                os.setText("");
                                tamano.setText("");
                            }
                        }
                        @Override
                        public void onFailure(Call<Messages> call, Throwable t) {
                        }
                    });
                }

            }
        });

    }
}