package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Activities.EditMainActivity;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Activities.RegisterMainActivity;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Connect;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.ConnectDb.Router;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.Messages;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.ModelPublications;
import facci.pm.moreira.ortiz.emilio.myapplicationaporte.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterRecycler extends RecyclerView.Adapter<AdapterRecycler.ViewHolder> {

    private ArrayList<ModelPublications> publications;
    private Context context;
    private Router router;

    public AdapterRecycler(ArrayList<ModelPublications> publications, Context context) {
        this.publications = publications;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.publications, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.dim(publications.get(position));
    }

    @Override
    public int getItemCount() {
        return publications.size();
    }


    class  ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre, correo, modelo;
        private ImageView perfil, delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            modelo = itemView.findViewById(R.id.modelo);
            nombre = itemView.findViewById(R.id.descrip);
            correo = itemView.findViewById(R.id.os);
            perfil = itemView.findViewById(R.id.profile_image);
            delete = itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    //enviar datos a la otra pantalla de editRegister
                    Intent intent = new Intent(context, EditMainActivity.class);
                    intent.putExtra("model", publications.get(position).getModel());
                    intent.putExtra("img",publications.get(position).getImg());
                    intent.putExtra("description",publications.get(position).getDescrip());
                    intent.putExtra("os",publications.get(position).getOs());
                    intent.putExtra("tamano",publications.get(position).getTamano());
                    context.startActivity(intent);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    router = Connect.getApiService();
                    Toast.makeText(context, "" + publications.get(getAdapterPosition()).getModel(), Toast.LENGTH_SHORT).show();
                    Call<Messages> messagesCall = router.eliminarPublications(publications.get(getAdapterPosition()).getModel());
                    messagesCall.enqueue(new Callback<Messages>() {
                        @Override
                        public void onResponse(Call<Messages> call, Response<Messages> response) {
                            Toast.makeText(context, "Eliminado", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Messages> call, Throwable t) {

                        }
                    });
                }
            });
        }
        public void dim(ModelPublications modelPublications){
            this.nombre.setText(modelPublications.getDescrip());
            this.correo.setText(modelPublications.getOs());
            this.modelo.setText((modelPublications.getModel()));
            Glide.with(context).load(modelPublications.getImg()).into(perfil);
        }
    }
}
