package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models;

public class CreatePublications {

    private String model;
    private String descrip;
    private String img;
    private String os;
    private String tamano;

    public CreatePublications(String model, String descrip, String img, String os, String tamano) {
        this.model = model;
        this.descrip = descrip;
        this.img = img;
        this.os = os;
        this.tamano = tamano;
    }
}
