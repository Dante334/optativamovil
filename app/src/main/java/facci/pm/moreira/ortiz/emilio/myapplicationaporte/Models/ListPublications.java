package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models.ModelPublications;

public class ListPublications {

    @SerializedName("publications")
    @Expose
    private ArrayList<ModelPublications> modelPublications = null;

    public ArrayList<ModelPublications> getModelPublications() {
        return modelPublications;
    }
}
