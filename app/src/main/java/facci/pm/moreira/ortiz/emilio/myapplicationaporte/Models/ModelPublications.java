package facci.pm.moreira.ortiz.emilio.myapplicationaporte.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelPublications {

    @SerializedName("descrip")
    @Expose
    private String descrip;

    @SerializedName("img")
    @Expose
    private String img;

    @SerializedName("os")
    @Expose
    private String os;

    @SerializedName("tamano")
    @Expose
    private String tamano;

    @SerializedName("model")
    @Expose
    private String model;

    public String getDescrip() {
        return descrip;
    }

    public String getImg() {
        return img;
    }

    public String getOs() {
        return os;
    }

    public String getTamano() {
        return tamano;
    }

    public String getModel() {
        return model;
    }
}
